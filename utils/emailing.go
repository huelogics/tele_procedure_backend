package utils

import (
	"fmt"
	"net/smtp"
	"os"
)

func SendEmailForTherapist(email, therapistId, USER string) error {
	username := os.Getenv("SMPTUSERNAME")
	password := os.Getenv("SMTPPASSWORD")
	host := os.Getenv("SMPTPHOST")
	auth := smtp.PlainAuth("", username, password, host)

	to := []string{email}
	from := username
	subject := "Your Therapist ID"
	body := fmt.Sprintf("Hi %s,\n\nYour Therapist ID is: %s\n\nKeep it safe for future use.\n\nThanks,\n[HUELOGICS]\n[TELE_PROCEDURE]", USER, therapistId)

	message := []byte("To: " + email + "\r\n" +
		"From: " + from + "\r\n" +
		"Subject: " + subject + "\r\n" +
		"\r\n" +
		body)

	err := smtp.SendMail(host+os.Getenv("SMTPPORT"), auth, from, to, message)
	if err != nil {
		return err
	}
	return nil
}
