package helpers

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"procedure-backend/httputil"
	"procedure-backend/initialisation"
	"procedure-backend/models"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v8"
	"github.com/golang-jwt/jwt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/crypto/bcrypt"
)

type SignedDetails struct {
	Uid string
	jwt.StandardClaims
}

func GenerateAccessToken(uid string) (string, error) {
	ACCESS_TOKEN_SECRET := os.Getenv("ACCESS_TOKEN_SECRET")
	if ACCESS_TOKEN_SECRET == "" {
		return "", errors.New("access token secret is not set")
	}
	accessClaims := &SignedDetails{
		Uid: uid,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 24).Unix(),
		},
	}
	token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, accessClaims).SignedString([]byte(ACCESS_TOKEN_SECRET))
	if err != nil {
		return "", err
	}

	return token, nil
}

func GenerateRefreshToken(uid string) (string, error) {
	REFRESH_TOKEN_SECRET := os.Getenv("REFRESH_TOKEN_SECRET")
	if REFRESH_TOKEN_SECRET == "" {
		return "", errors.New("refresh token secret is not set")
	}
	refreshClaims := &SignedDetails{
		Uid: uid,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 24 * 7).Unix(),
		},
	}
	token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, refreshClaims).SignedString([]byte(REFRESH_TOKEN_SECRET))
	if err != nil {
		return "", err
	}

	return token, nil
}

func ValidateToken(signedToken, secretKey string) (*SignedDetails, error) {
	if secretKey == "" {
		return nil, errors.New("secret key is not set")
	}
	token, err := jwt.ParseWithClaims(signedToken, &SignedDetails{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(secretKey), nil
	})

	if err != nil {
		return nil, fmt.Errorf("failed to parse token: %w", err)
	}

	claims, ok := token.Claims.(*SignedDetails)
	if !ok {
		return nil, errors.New("invalid token claims")
	}

	if claims.ExpiresAt < time.Now().Unix() {
		return nil, errors.New("token expired")
	}

	return claims, nil
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

func VerifyPassword(userPassword string, providedPassword string) (bool, string) {
	// to compare the provided password and the stored user password
	err := bcrypt.CompareHashAndPassword([]byte(userPassword), []byte(providedPassword))

	check := true
	msg := ""

	if err != nil {
		fmt.Println("error", err)
		msg = fmt.Sprintln("password is incorrect")
		check = false
	}
	return check, msg
}

func Retrievetoken(tokenName string, c *gin.Context) (string, error) {
	token := c.Request.Header.Get("Authorization")
	if token == "" {
		cookie, err := c.Request.Cookie(tokenName)
		if err != nil {
			c.JSON(http.StatusUnauthorized, httputil.HTTPMessage{Code: http.StatusUnauthorized, Message: "Unauthorized User: Cookie not present"})
			return "", err
		}
		token = cookie.Value
	} else {
		token = strings.TrimPrefix(token, "Bearer ")
	}

	if token == "" {
		err := fmt.Errorf("token not found")
		c.JSON(http.StatusUnauthorized, httputil.HTTPMessage{Code: http.StatusUnauthorized, Message: err})
		return "", err
	}
	return token, nil
}
func RetrieveUserFromCookie(c *gin.Context) (*models.InternalTherapist, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	user, exists := c.Get("user")
	if !exists {
		SendErrorResponse(c, http.StatusUnauthorized, "Unauthorized Request")
		return nil, fmt.Errorf("unauthorized request")
	}

	internalTherapist, ok := user.(models.InternalTherapist)
	if !ok {
		SendErrorResponse(c, http.StatusInternalServerError, "Internal error: invalid user type")
		return nil, fmt.Errorf("invalid user type")
	}
	a := internalTherapist.ID.Hex()
	redisKey := fmt.Sprintf("therapist:%s", a)
	val, err := initialisation.RedisClient.Get(ctx, redisKey).Result()
	if err == redis.Nil {
		foundInternalTherapist, err := FindTherapistInDB(ctx, internalTherapist.ID)
		if err != nil {
			if err == mongo.ErrNoDocuments {
				SendErrorResponse(c, http.StatusNotFound, "Therapist doesn't exist")
			} else {
				SendErrorResponse(c, http.StatusInternalServerError, "Error finding therapist")
			}
			return nil, err
		}
		err = CacheTherapistInRedis(ctx, redisKey, foundInternalTherapist)
		if err != nil {
			SendErrorResponse(c, http.StatusInternalServerError, "Error storing therapist data in Redis")
			return nil, err
		}
		return foundInternalTherapist, nil
	} else if err != nil {
		SendErrorResponse(c, http.StatusInternalServerError, "Error accessing Redis")
		return nil, err
	}

	var foundInternalTherapist models.InternalTherapist
	err = json.Unmarshal([]byte(val), &foundInternalTherapist)
	if err != nil {
		SendErrorResponse(c, http.StatusInternalServerError, "Error decoding therapist data")
		return nil, err
	}

	return &foundInternalTherapist, nil
}

func SendErrorResponse(c *gin.Context, code int, message string) {
	c.JSON(code, httputil.HTTPMessage{Code: code, Message: message})
}

func FindTherapistInDB(ctx context.Context, id primitive.ObjectID) (*models.InternalTherapist, error) {
	var foundInternalTherapist models.InternalTherapist
	filter := bson.M{"_id": id}
	err := initialisation.InternalTherapistCollection.FindOne(ctx, filter).Decode(&foundInternalTherapist)
	if err != nil {
		return nil, err
	}
	return &foundInternalTherapist, nil
}

func CacheTherapistInRedis(ctx context.Context, key string, therapist *models.InternalTherapist) error {
	data, err := json.Marshal(therapist)
	if err != nil {
		return err
	}
	return initialisation.RedisClient.Set(ctx, key, data, 1*time.Hour).Err()
}

func NextSequenceNumber(c *gin.Context) (int64, error) {
	filter := bson.M{"_id": "case_number"}
	update := bson.M{"$inc": bson.M{"value": 1}}

	var counter models.Counter
	options := options.FindOneAndUpdate().SetReturnDocument(options.After).SetUpsert(true)

	err := initialisation.CounterCollection.FindOneAndUpdate(context.TODO(), filter, update, options).Decode(&counter)

	if err != nil {
		c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: err.Error()})
		return 0, err
	}
	return counter.Value, nil
}
