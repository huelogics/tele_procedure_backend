package initialisation

import (
	"log"
	"procedure-backend/database"

	"github.com/go-playground/validator"
	"github.com/go-redis/redis/v8"
	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/mongo"
)

var (
	InternalTherapistCollection *mongo.Collection
	ProcedureCollection         *mongo.Collection
	CvProcedureCollection       *mongo.Collection
	RecordCollection            *mongo.Collection
	ExerciseCollection          *mongo.Collection
	CounterCollection           *mongo.Collection
	Client                      *mongo.Client
	Validate                    = validator.New()
	RedisClient                 *redis.Client
)

func init() {
	err := godotenv.Load("./.env")
	if err != nil {
		log.Fatalf("Error loading .env file: %v", err)
	}
	Client = database.DBinstance()
	InternalTherapistCollection = database.OpenCollection(Client, "internalTherapistCollection")
	ProcedureCollection = database.OpenCollection(Client, "procedureCollection")
	CvProcedureCollection = database.OpenCollection(Client, "cvprocedureCollection")
	RecordCollection = database.OpenCollection(Client, "ProcessCaseCollection")
	ExerciseCollection = database.OpenCollection(Client, "exerciseCollection")
	CounterCollection = database.OpenCollection(Client, "counters")
	RedisClient = database.ConnectToRedis()
}
