package middleware

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"procedure-backend/helpers"
	"procedure-backend/httputil"
	"procedure-backend/initialisation"
	"procedure-backend/models"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v8"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func CorsSettings(router *gin.Engine) {
	router.Use(gin.Logger())
	router.Use(gin.Recovery())
	router.Use(cors.New(cors.Config{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"},
		AllowHeaders:     []string{"Origin", "Accept", "Content-Type", "Authorization", "Upgrade", "Connection"},
		AllowCredentials: true,
	}))

}

func Authenticate() gin.HandlerFunc {
	return func(c *gin.Context) {
		token, err := helpers.Retrievetoken("accesstoken", c)
		if err != nil || token == "" {
			c.AbortWithStatusJSON(http.StatusUnauthorized, httputil.HTTPMessage{Code: http.StatusUnauthorized, Message: "Missing or invalid token"})
			return
		}

		claims, err := helpers.ValidateToken(token, os.Getenv("ACCESS_TOKEN_SECRET"))
		if err != nil {
			c.JSON(http.StatusUnauthorized, httputil.HTTPMessage{Code: http.StatusUnauthorized, Message: err.Error()})
			c.Abort()
			return
		}

		userId, err := primitive.ObjectIDFromHex(claims.Uid)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Invalid user ID"})
			return
		}

		therapist, err := RetrieveOrStoreUserDataInRedis(userId.Hex(), c)
		if err != nil {
			c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: err.Error()})
			c.Abort()
			return
		}

		c.Set("user", *therapist)
		c.Next()
	}
}

func RetrieveOrStoreUserDataInRedis(userId string, c *gin.Context) (*models.InternalTherapist, error) {
	var therapist models.InternalTherapist
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	redisKey := fmt.Sprintf("therapist:%s", userId)
	val, err := initialisation.RedisClient.Get(ctx, redisKey).Result()
	if err == redis.Nil {
		opts := options.FindOne().SetProjection(bson.M{"password": 0})
		id, err := primitive.ObjectIDFromHex(userId)
		if err != nil {
			return nil, fmt.Errorf("invalid user ID: %v", err)
		}
		err = initialisation.InternalTherapistCollection.FindOne(ctx, bson.M{"_id": id}, opts).Decode(&therapist)
		if err != nil {
			if err == mongo.ErrNoDocuments {
				return nil, fmt.Errorf("user not found in database")
			}
			return nil, fmt.Errorf("database error: %v", err)
		}

		data, err := json.Marshal(therapist)
		if err != nil {
			return nil, fmt.Errorf("failed to marshal user data: %v", err)
		}
		if err := initialisation.RedisClient.Set(ctx, redisKey, data, 1*time.Hour).Err(); err != nil {
			return nil, fmt.Errorf("failed to set user data in Redis: %v", err)
		}
		return &therapist, nil

	} else if err != nil {
		return nil, fmt.Errorf("failed to get user data from Redis: %v", err)
	}

	err = json.Unmarshal([]byte(val), &therapist)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal user data: %v", err)
	}

	return &therapist, nil
}
