package main

import (
	"os"
	_ "procedure-backend/docs"
	_ "procedure-backend/initialisation"
	"procedure-backend/middleware"
	"procedure-backend/routes"

	"github.com/gin-gonic/gin"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// @title Tele-Procedure API
// @version 1.0
// @description This is a sample server for managing InternalTherapist.

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io
// @host https://procedures.huelogics.com
// @BasePath /

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "4011"
	}

	router := gin.New()
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))
	middleware.CorsSettings(router)
	routes.ProcedureRoutes(router)

	router.Run(":" + port)
}
