package controllers

import (
	"context"
	"errors"
	"log"
	"net/http"
	"os"
	"procedure-backend/helpers"
	"procedure-backend/httputil"
	initialisation "procedure-backend/initialisation"
	"procedure-backend/models"
	"procedure-backend/utils"
	"time"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// SignUpHandler godoc
// @Summary Create New Account
// @Description InternalTherapist Will Create Account
// @Tags InternalTherapist
// @Accept application/json
// @Produce application/json
// @Param internalTherapist body models.InternalTherapist true "InternalTherapist object"
// @Success 201 {object} models.InternalTherapist
// @Failure 400 {object} httputil.HTTPMessage
// @Failure 500 {object} httputil.HTTPMessage
// @Router /sign_up [post]
func SignUpHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		var therapist models.InternalTherapist
		if err := c.BindJSON(&therapist); err != nil {
			c.JSON(http.StatusBadRequest, httputil.HTTPMessage{Code: http.StatusBadRequest, Message: err.Error()})
			return
		}

		if validationErr := initialisation.Validate.Struct(therapist); validationErr != nil {
			c.JSON(http.StatusBadRequest, httputil.HTTPMessage{Code: http.StatusBadRequest, Message: validationErr.Error()})
			return
		}

		err := initialisation.InternalTherapistCollection.FindOne(ctx, bson.M{"email": therapist.Email}).Decode(&therapist)
		if err == nil {
			c.JSON(http.StatusBadRequest, httputil.HTTPMessage{Code: http.StatusBadRequest, Message: "Email Already Exists"})
			return
		} else {
			if !errors.Is(err, mongo.ErrNoDocuments) {
				c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: err.Error()})
				return
			}
		}

		// Hash password
		hashedPassword, err := helpers.HashPassword(therapist.Password)
		if err != nil {
			c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: "Failed To Hash Password"})
			return
		}

		therapist.ID = primitive.NewObjectID()
		therapistId, err := utils.GenerateID()
		if err != nil {
			c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: "Error While Generating Therapist ID"})
		}
		therapist.TherapistID = therapistId
		therapist.Password = hashedPassword
		therapist.CreatedAt = time.Now()
		therapist.UpdatedAt = time.Now()
		therapist.IsSuperUser = false

		_, insertErr := initialisation.InternalTherapistCollection.InsertOne(ctx, therapist)
		if insertErr != nil {
			msg := "Therapist was not created"
			c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: msg})
			log.Println(msg)
			return
		}
		utils.SendEmailForTherapist(therapist.Email, therapistId, therapist.Name)
		therapist.Password = ""

		c.JSON(http.StatusCreated, therapist)
	}
}

// SignInHandler godoc
// @Summary Login APi
// @Description InternalTherapist Will login through Emails and Password
// @Tags InternalTherapist
// @Accept application/json
// @Produce application/json
// @Param internalTherapist body models.LoginInfoData true "InternalTherapist object"
// @Success 200 {object} models.TokenResponse
// @Failure 400 {object} httputil.HTTPMessage
// @Failure 500 {object} httputil.HTTPMessage
// @Failure 404 {object} httputil.HTTPMessage
// @Router /sign_in [post]
func SignInHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		var therapist models.LoginInfoData
		var foundTherapist models.InternalTherapist
		if err := c.BindJSON(&therapist); err != nil {
			c.JSON(http.StatusBadRequest, httputil.HTTPMessage{Code: http.StatusBadRequest, Message: err.Error()})
			return
		}

		err := initialisation.InternalTherapistCollection.FindOne(ctx, bson.M{"email": therapist.Email}).Decode(&foundTherapist)
		if err != nil {
			c.JSON(http.StatusNotFound, httputil.HTTPMessage{Code: http.StatusNotFound, Message: "Please Provide a Valid Email"})
			return
		}

		passwordIsValid, msg := helpers.VerifyPassword(foundTherapist.Password, therapist.Password)
		if !passwordIsValid {
			c.JSON(http.StatusBadRequest, httputil.HTTPMessage{Code: http.StatusBadRequest, Message: msg})
			return
		}

		accessToken, err := helpers.GenerateAccessToken(foundTherapist.ID.Hex())
		if err != nil {
			c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: "Error occurred while generating Access Token"})
			return
		}

		refreshToken, err := helpers.GenerateRefreshToken(foundTherapist.ID.Hex())
		if err != nil {
			c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: "Error occurred while generating Refresh Token"})
			return
		}
		c.SetCookie("accesstoken", accessToken, 24*60*60, "/", "procedures.huelogics.com", true, true)
		c.SetCookie("refreshtoken", refreshToken, 7*24*60*60, "/", "procedures.huelogics.com", true, true)

		filter := bson.M{"_id": foundTherapist.ID}
		update := bson.M{"$set": bson.M{"refreshtoken": refreshToken}}

		res, err := initialisation.InternalTherapistCollection.UpdateOne(ctx, filter, update, options.Update().SetUpsert(true))
		if err != nil {
			c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: err.Error()})
			return
		}

		if res.ModifiedCount == 0 {
			c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: "Failed to Update refresh token"})
			return
		}
		response := models.TokenResponse{
			AccessToken:  accessToken,
			RefreshToken: refreshToken,
		}
		c.JSON(http.StatusOK, response)
	}
}

// SignOutHandler godoc
// @Summary Logout
// @Description InternalTherapist Will logout session
// @Tags InternalTherapist
// @Produce application/json
// @Success 200 {object} httputil.HTTPMessage
// @Failure 401 {object} httputil.HTTPMessage
// @Failure 500 {object} httputil.HTTPMessage
// @Router /sign_out [post]
// @Security BearerAuth
func SignOutHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		user, exists := c.Get("user")
		if !exists {
			c.JSON(http.StatusUnauthorized, httputil.HTTPMessage{Code: http.StatusUnauthorized, Message: "Unauthorized request"})
			return
		}
		internalTherapist, ok := user.(models.InternalTherapist)
		if !ok {
			c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: "Internal error: invalid user type"})
			return
		}
		filter := bson.M{"_id": internalTherapist.ID}
		_, err := initialisation.InternalTherapistCollection.UpdateOne(ctx, filter, bson.M{"$unset": bson.M{"refreshtoken": 1}})
		if err != nil {
			c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: err.Error()})
			return
		}
		c.SetCookie("accesstoken", "", -1, "/", "procedures.huelogics.com", false, true)
		c.SetCookie("refreshtoken", "", -1, "/", "procedures.huelogics.com", false, true)
		c.JSON(http.StatusOK, httputil.HTTPMessage{Code: http.StatusOK, Message: "Logout Successful"})
	}
}

func RefreshTokenHandler() gin.HandlerFunc {
	return func(c *gin.Context) {

		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		token, err := helpers.Retrievetoken("refreshtoken", c)

		if err != nil || token == "" {
			c.JSON(http.StatusUnauthorized, httputil.HTTPMessage{Code: http.StatusUnauthorized, Message: err.Error()})
			return
		}
		REFRESH_TOKEN_SECRET := os.Getenv("REFRESH_TOKEN_SECRET")
		claims, err := helpers.ValidateToken(token, REFRESH_TOKEN_SECRET)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
			return
		}
		var therapist models.InternalTherapist
		projectionOpts := options.FindOne().SetProjection(bson.M{
			"password":     0,
			"therapist_id": 0,
		})
		userid, err := primitive.ObjectIDFromHex(claims.Uid)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
			return
		}
		err = initialisation.InternalTherapistCollection.FindOne(ctx, bson.M{"_id": userid}, projectionOpts).Decode(&therapist)
		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
			return
		}
		if token != therapist.RefreshToken {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Refresh token is expired or used Please log in Again"})
			return
		}

		accesstoken, err := helpers.GenerateAccessToken(therapist.ID.Hex())
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Error while Generating access token"})
			return
		}
		refreshtoken, err := helpers.GenerateRefreshToken(therapist.ID.Hex())
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Error while Generating Refresh token"})
			return
		}

		filter := bson.M{"_id": primitive.ObjectID(therapist.ID)}
		update := bson.M{"$set": bson.M{"refresh_token": refreshtoken}}
		updateOpts := options.Update().SetUpsert(false)

		result, err := initialisation.InternalTherapistCollection.UpdateOne(ctx, filter, update, updateOpts)

		if err != nil {
			c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: err.Error()})
			return
		}
		if result.ModifiedCount == 0 {
			c.JSON(http.StatusNotFound, gin.H{"error": "Therapist not found"})
			return
		}
		c.SetCookie("accessToken", accesstoken, int(time.Hour.Seconds()), "/", "procedures.huelogics.com", true, true)
		c.SetCookie("refreshToken", refreshtoken, int(7*24*time.Hour.Seconds()), "/", "procedures.huelogics.com", true, true)
		c.JSON(http.StatusOK, gin.H{
			"user":         therapist,
			"accessToken":  accesstoken,
			"refreshToken": refreshtoken,
			"message":      "Access token refreshed",
		})
	}
}
