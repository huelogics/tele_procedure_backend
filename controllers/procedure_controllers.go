package controllers

import (
	"context"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"procedure-backend/helpers"
	"procedure-backend/httputil"
	initialisation "procedure-backend/initialisation"
	"procedure-backend/models"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

// AddProcedureHandler godoc
// @Summary Add Procedure
// @Description InternalTherapist Will add NewProcedure
// @Tags Procedure
// @Accept application/json
// @Produce application/json
// @Param procedure body models.Procedure true "Procedure object"
// @Success 201 {object} httputil.HTTPMessage
// @Failure 400 {object} httputil.HTTPMessage
// @Failure 401 {object} httputil.HTTPMessage
// @Failure 404 {object} httputil.HTTPMessage
// @Failure 500 {object} httputil.HTTPMessage
// @Router /add_procedure [post]
// @Security BearerAuth
func AddProcedureHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		therapist, err := helpers.RetrieveUserFromCookie(c)
		if err != nil {
			return
		}

		var procedure models.Procedure
		if err := c.BindJSON(&procedure); err != nil {
			c.JSON(http.StatusBadRequest, httputil.HTTPMessage{Code: http.StatusBadRequest, Message: err.Error()})
			return
		}

		validationErr := initialisation.Validate.Struct(procedure)
		if validationErr != nil {
			c.JSON(http.StatusBadRequest, httputil.HTTPMessage{Code: http.StatusBadRequest, Message: validationErr.Error()})
			return
		}

		procedure.ID = primitive.NewObjectID()
		procedure.ProcedureId = procedure.ID.Hex()
		procedure.TherapistId = therapist.TherapistID

		result, insertErr := initialisation.ProcedureCollection.InsertOne(ctx, procedure)
		if insertErr != nil {
			msg := fmt.Sprintln("Procedure data was not inserted")
			c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: msg})
			return

		}

		c.JSON(http.StatusCreated, httputil.HTTPMessage{Code: http.StatusCreated, Message: result.InsertedID})

	}
}

// GetAllProceduresHandler godoc
// @Summary Get all procedures
// @Description Retrieves all procedures from the database
// @Tags Procedure
// @Accept json
// @Produce json
// @Success 200 {array} models.ProcedureResponse
// @Failure 500 {object} httputil.HTTPMessage
// @Failure 401 {object} httputil.HTTPMessage
// @Router /get_all_procedures [get]
// @Security BearerAuth
func GetAllProceduresHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		result, err := initialisation.ProcedureCollection.Find(ctx, bson.M{})
		if err != nil {
			c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: err.Error()})
			return
		}
		defer result.Close(ctx)

		var allProcedures []models.Procedure
		if err := result.All(ctx, &allProcedures); err != nil {
			c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: err.Error()})
			return
		}

		// Map the results to the new struct
		var responseProcedures []models.ProcedureResponse
		for _, procedure := range allProcedures {
			responseProcedures = append(responseProcedures, models.ProcedureResponse{
				Name:          procedure.Name,
				Options:       procedure.Options,
				Landmarks:     procedure.Landmarks,
				Joints:        procedure.Joints,
				Details:       procedure.Details,
				IdealRange:    procedure.IdealRange,
				PivotLandmark: procedure.PivotLandmark,
			})
		}

		c.JSON(http.StatusOK, responseProcedures)

	}
}

func GetProcedureHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		procedureId := c.Param("procedure_id")

		var procedure models.Procedure

		err := initialisation.ProcedureCollection.FindOne(ctx, bson.M{"procedure_id": procedureId}).Decode(&procedure)
		if err != nil {
			if err == mongo.ErrNoDocuments {
				c.JSON(http.StatusBadRequest, gin.H{"error": "Procedure was not found"})
				return
			}
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		c.JSON(http.StatusOK, procedure)
	}
}

func CVProcedureHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		procedureId := c.Param("procedure_id")

		var procedure models.Procedure
		var cvprocedure models.CVProcedure
		var optionType models.OptionType

		err := initialisation.ProcedureCollection.FindOne(ctx, bson.M{"procedure_id": procedureId}).Decode(&procedure)
		if err != nil {
			if err == mongo.ErrNoDocuments {
				c.JSON(http.StatusBadRequest, gin.H{"error": "Procedure was not found"})
				return
			}
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		options := procedure.Options

		for _, option := range options {
			optionType.Key = option
			var landmarkJoint models.LandmarkJoint
			landmarkJoint.Key = option + "_" + procedure.Name + "_joints"
			for _, joint := range procedure.Joints {
				joint1 := joint[0]
				joint2 := joint[1]
				if strings.HasPrefix(joint1, option) && strings.HasPrefix(joint2, option) {
					jointString := "(mp_pose.PoseLandmark." + joint1 + ".value,mp_pose.PoseLandmark." + joint2 + ".value)"
					landmarkJoint.Value = append(landmarkJoint.Value, jointString)
				}
			}

			for _, coordinate := range procedure.Landmarks {
				landmark := coordinate
				if strings.HasPrefix(landmark, option) {
					coordinateString := landmark + "=[landmarks[mp_pose.PoseLandmark." + landmark + ".value].x,landmarks[mp_pose.PoseLandmark." + landmark + ".value].y]"
					landmarkJoint.Coordinates = append(landmarkJoint.Coordinates, coordinateString)
				}
			}

			optionType.Value = landmarkJoint

			cvprocedure.Type = append(cvprocedure.Type, optionType)

		}

		cvprocedure.ID = primitive.NewObjectID()
		cvprocedure.CVProcedureId = cvprocedure.ID.Hex()

		_, insertErr := initialisation.CvProcedureCollection.InsertOne(ctx, cvprocedure)
		if insertErr != nil {
			msg := fmt.Sprintln("CVProcedure data was not inserted")
			c.JSON(http.StatusInternalServerError, gin.H{"error": msg})
			return

		}

		c.JSON(http.StatusOK, cvprocedure)
	}
}

func GetAllCVProceduresHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		result, err := initialisation.CvProcedureCollection.Find(ctx, bson.M{})
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		defer result.Close(ctx)

		var allCVProcedures []models.CVProcedure
		if err := result.All(ctx, &allCVProcedures); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		c.JSON(http.StatusOK, allCVProcedures)
	}
}

func ExportCVProceduresToCSVHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		// Query CVProcedure data
		cvProcedures := []models.CVProcedure{}
		cursor, err := initialisation.CvProcedureCollection.Find(ctx, bson.M{})
		if err != nil {
			print("here1")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		defer cursor.Close(ctx)
		if err := cursor.All(ctx, &cvProcedures); err != nil {
			print("here2")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		// Create CSV writer
		csvFile, err := os.Create("exported_data.csv")
		if err != nil {
			print("here3")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		defer csvFile.Close()
		writer := csv.NewWriter(csvFile)
		defer writer.Flush()

		// Write header
		header := []string{"CVProcedure ID", "Procedure ID", "Procedure Name", "Options", "Landmarks", "Joints", "Therapist ID", "Details", "Ideal Range", "Pivot Landmark"}
		if err := writer.Write(header); err != nil {
			print("here4")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		// Write CVProcedure data along with corresponding Procedure data
		for _, cvProcedure := range cvProcedures {
			for _, optionType := range cvProcedure.Type {
				row := []string{
					cvProcedure.CVProcedureId,
					optionType.Key,
				}

				// Query corresponding Procedure data
				var procedure models.Procedure
				err := initialisation.ProcedureCollection.FindOne(ctx, bson.M{"procedure_id": cvProcedure.ProcedureId}).Decode(&procedure)
				if err != nil {
					print("here5")
					c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
					return
				}

				// Append Procedure data to the row
				row = append(row,
					procedure.Name,
					strings.Join(procedure.Options, ";"),
					strings.Join(procedure.Landmarks, ";"),
					formatJoints(procedure.Joints),
					procedure.Details,
					fmt.Sprintf("%f", procedure.IdealRange),
					procedure.PivotLandmark,
					optionType.Key,
					optionType.Value.Key,
					strings.Join(optionType.Value.Coordinates, ";"),
				)

				// Write row to CSV file
				if err := writer.Write(row); err != nil {
					c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
					return
				}
			}
		}

		c.File("exported_data.csv")
	}
}

// Function to format joints
func formatJoints(joints [][]string) string {
	formattedJoints := []string{}
	for _, joint := range joints {
		formattedJoints = append(formattedJoints, fmt.Sprintf("[%s,%s]", joint[0], joint[1]))
	}
	return strings.Join(formattedJoints, ";")
}

func ExportCVProceduresToJSONHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		// Query CVProcedure data
		cvProcedures := []models.CVProcedure{}
		cursor, err := initialisation.CvProcedureCollection.Find(ctx, bson.M{})
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		defer cursor.Close(ctx)
		if err := cursor.All(ctx, &cvProcedures); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		// Prepare JSON data
		var jsonData []byte
		jsonData, err = json.MarshalIndent(cvProcedures, "", "    ")
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		// Set response headers
		c.Header("Content-Description", "File Transfer")
		c.Header("Content-Disposition", "attachment; filename=exported_data.json")
		c.Data(http.StatusOK, "application/json", jsonData)
	}
}
