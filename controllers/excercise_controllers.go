package controllers

import (
	"context"
	"fmt"
	"net/http"
	"procedure-backend/helpers"
	"procedure-backend/initialisation"
	"procedure-backend/models"
	"time"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func AddExerciseHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		var exercise models.Exercise

		if err := c.BindJSON(&exercise); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		validationErr := initialisation.Validate.Struct(exercise)
		if validationErr != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": validationErr.Error()})
			return
		}
		internaltherapist, err := helpers.RetrieveUserFromCookie(c)
		if err != nil {
			return
		}
		exercise.TherapistID = internaltherapist.TherapistID
		exercise.ID = primitive.NewObjectID()
		exercise.ExerciseId = exercise.ID.Hex()

		result, insertErr := initialisation.ExerciseCollection.InsertOne(ctx, exercise)
		if insertErr != nil {
			msg := fmt.Sprintln("Exercise data was not inserted")
			c.JSON(http.StatusInternalServerError, gin.H{"error": msg})
			return

		}

		c.JSON(http.StatusOK, result)

	}
}

func GetAllExercisesHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		result, err := initialisation.ExerciseCollection.Find(ctx, bson.M{})
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		defer result.Close(ctx)

		var allExercises []models.Exercise
		if err := result.All(ctx, &allExercises); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		c.JSON(http.StatusOK, allExercises)
	}
}

func GetExerciseHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		exerciseId := c.Param("exercise_id")

		var exercise models.Exercise

		err := initialisation.ExerciseCollection.FindOne(ctx, bson.M{"exercise_id": exerciseId}).Decode(&exercise)
		if err != nil {
			if err == mongo.ErrNoDocuments {
				c.JSON(http.StatusBadRequest, gin.H{"error": "Exercise was not found"})
				return
			}
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		c.JSON(http.StatusOK, exercise)
	}
}

func EditExerciseHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		exerciseId := c.Param("exercise_id")
		if exerciseId == "" {
			c.JSON(http.StatusBadRequest, gin.H{"error": "exercise id is required"})
			return
		}

		var updatedExercise models.Exercise

		if err := c.BindJSON(&updatedExercise); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		validationErr := initialisation.Validate.Struct(updatedExercise)
		if validationErr != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": validationErr.Error()})
			return
		}

		internaltherapist, err := helpers.RetrieveUserFromCookie(c)
		if err != nil {
			return
		}
		updatedExercise.TherapistID = internaltherapist.TherapistID
		updatedExercise.ExerciseId = exerciseId

		filter := bson.M{"exercise_id": exerciseId, "therapist_id": internaltherapist.TherapistID}
		update := bson.M{

			"$set": updatedExercise,
		}

		result, err := initialisation.ExerciseCollection.UpdateOne(ctx, filter, update)
		if err != nil {
			msg := fmt.Sprintln("Failed to update exercise")
			c.JSON(http.StatusInternalServerError, gin.H{"error": msg})
			return
		}

		if result.MatchedCount == 0 {
			msg := fmt.Sprintln("No exercise found to update")
			c.JSON(http.StatusNotFound, gin.H{"error": msg})
			return
		}

		c.JSON(http.StatusOK, gin.H{"message": "Exercise updated successfully"})
	}
}
