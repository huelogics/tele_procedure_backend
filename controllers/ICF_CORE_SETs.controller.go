package controllers

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"procedure-backend/helpers"
	"procedure-backend/httputil"
	initialisation "procedure-backend/initialisation"
	"procedure-backend/models"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// CreateICFCORESETRecord godoc
// @Summary Save CoreSet and ICF_Records
// @Description API to Save CoreSet and ICF_Records
// @Tags Records
// @Accept application/json
// @Produce application/json
// @Param text body models.Prompt true "Text data"
// @Success 200 {object} httputil.HTTPMessage
// @Failure 400 {object} httputil.HTTPMessage
// @Failure 500 {object} httputil.HTTPMessage
// @Router /create_icf_core_set_record [post]
// @Security BearerAuth
func CreateICFCORESETRecord() gin.HandlerFunc {
	return func(c *gin.Context) {

		var text models.Prompt
		if err := c.BindJSON(&text); err != nil {
			c.JSON(http.StatusBadRequest, httputil.HTTPMessage{Code: http.StatusBadRequest, Message: err.Error()})
			return
		}
		if validationErr := initialisation.Validate.Struct(text); validationErr != nil {
			c.JSON(http.StatusBadRequest, httputil.HTTPMessage{Code: http.StatusBadRequest, Message: validationErr.Error()})
			return
		}
		callForProcessAPI(c, text.Text)
	}
}

func callForProcessAPI(c *gin.Context, prompt string) {
	var payload bytes.Buffer
	writer := multipart.NewWriter(&payload)
	part, err := writer.CreateFormField("text")
	if err != nil {
		c.JSON(http.StatusBadRequest, httputil.HTTPMessage{Code: http.StatusBadRequest, Message: err.Error()})
		return
	}
	part.Write([]byte(prompt))

	err = writer.Close()
	if err != nil {
		c.JSON(http.StatusBadRequest, httputil.HTTPMessage{Code: http.StatusBadRequest, Message: err.Error()})
		return
	}

	req, err := http.NewRequest("POST", os.Getenv("API_URL"), &payload)
	if err != nil {
		c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: err.Error()})
		return
	}
	req.Header.Set("Content-Type", writer.FormDataContentType())

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: err.Error()})
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		c.JSON(resp.StatusCode, httputil.HTTPMessage{Code: resp.StatusCode, Message: "Error while making request to the API"})
		return
	}

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: err.Error()})
		return
	}

	var record map[string]interface{}
	err = json.Unmarshal(data, &record)
	if err != nil {
		c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: err.Error()})
		return
	}

	coreSets, coreSetsOk := record["Core_sets"]
	icfCase, icfCaseOk := record["ICF_case"]

	if !coreSetsOk || !icfCaseOk {
		c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: "Invalid JSON structure"})
		return
	}
	therapist, err := helpers.RetrieveUserFromCookie(c)
	if err != nil {
		return
	}
	nextnumer, err := helpers.NextSequenceNumber(c)
	if err != nil {
		return
	}
	recordData := models.Record{
		CaseNumber: nextnumer,
		Case:       prompt,
		CoreSets:   coreSets,
		ICF_CASE:   icfCase,
		Therapist:  therapist.TherapistID,
	}

	_, err = initialisation.RecordCollection.InsertOne(context.Background(), recordData)
	if err != nil {
		c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: err.Error()})
		return
	}
	c.JSON(http.StatusOK, httputil.HTTPMessage{Code: http.StatusOK, Message: nextnumer})
}

func GetRecordsByCaseNumber() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		caseNumberStr := c.Param("case_number")
		caseNumber, err := strconv.ParseInt(caseNumberStr, 10, 64)
		if err != nil {
			c.JSON(http.StatusBadRequest, httputil.HTTPMessage{Code: http.StatusBadRequest, Message: "Invalid case number"})
			return
		}

		var record models.Record

		if err := initialisation.RecordCollection.FindOne(ctx, bson.M{"case_number": caseNumber}).Decode(&record); err != nil {
			if err == mongo.ErrNoDocuments {
				c.JSON(http.StatusNotFound, httputil.HTTPMessage{Code: http.StatusNotFound, Message: "Record not found"})
			} else {
				c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: err.Error()})
			}
			return
		}

		c.JSON(http.StatusOK, record)
	}
}

// GetRecordsAddedByTherapist godoc
// @Summary Fetch records
// @Description Therapist and Super Therapist will fetch all the records added by him
// @Tags Records
// @Produce application/json
// @Success 200 {object} []models.Record
// @Failure 500 {object} httputil.HTTPMessage
// @Failure 404 {object} httputil.HTTPMessage
// @Failure 401 {object} httputil.HTTPMessage
// @Router /get_records_added_by_therapist [get]
// @Security BearerAuth
func GetRecordsAddedByTherapist() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		therapist, err := helpers.RetrieveUserFromCookie(c)
		if err != nil {
			return
		}
		var records []models.Record

		var filter primitive.M
		if therapist.IsSuperUser {
			filter = bson.M{}
		} else {
			filter = bson.M{"therapist": therapist.TherapistID}
		}

		cursor, err := initialisation.RecordCollection.Find(ctx, filter)

		if err != nil {
			c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: err.Error()})
			return
		}

		err = cursor.All(ctx, &records)
		if err != nil {
			c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: err.Error()})
			return
		}
		c.JSON(http.StatusOK, records)

	}
}

// UpdateRecordsAddedByTherapistAndSuperUser godoc
// @Summary Update a record by case number
// @Description Update an existing record by providing the case number and new data
// @Tags Records
// @Accept  json
// @Produce  json
// @Param case_number path int true "Case Number"
// @Param record body models.Record true "Record Data"
// @Success 200 {object} httputil.HTTPMessage
// @Failure 400 {object} httputil.HTTPMessage
// @Failure 404 {object} httputil.HTTPMessage
// @Failure 500 {object} httputil.HTTPMessage
// @Router /update_record_by_case_number/{case_number} [put]
func UpdateRecordsAddedByTherapistAndSuperUser() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		caseNumberStr := c.Param("case_number")
		caseNumber, err := strconv.ParseInt(caseNumberStr, 10, 64)
		if err != nil {
			c.JSON(http.StatusBadRequest, httputil.HTTPMessage{Code: http.StatusBadRequest, Message: "Invalid case number"})
			return
		}

		var updatedRecord models.Record
		if err := c.ShouldBindJSON(&updatedRecord); err != nil {
			c.JSON(http.StatusBadRequest, httputil.HTTPMessage{Code: http.StatusBadRequest, Message: "Invalid request data"})
			return
		}

		filter := bson.M{"case_number": caseNumber}
		update := bson.M{"$set": updatedRecord}

		opts := options.Update().SetUpsert(false)
		result, err := initialisation.RecordCollection.UpdateOne(ctx, filter, update, opts)
		if err != nil {
			c.JSON(http.StatusInternalServerError, httputil.HTTPMessage{Code: http.StatusInternalServerError, Message: "Error updating record"})
			return
		}

		if result.MatchedCount == 0 {
			c.JSON(http.StatusNotFound, httputil.HTTPMessage{Code: http.StatusNotFound, Message: "Record not found"})
			return
		}

		c.JSON(http.StatusOK, httputil.HTTPMessage{Message: "Record updated successfully"})
	}
}
