package models

type Record struct {
	CaseNumber int64       `bson:"case_number"`
	Case       string      `bson:"case"`
	CoreSets   interface{} `bson:"core_sets"`
	ICF_CASE   interface{} `bson:"icf_case"`
	Therapist  string      `bson:"therapist"`
}
type Prompt struct {
	Text string `json:"prompt" validate:"required"`
}

type Counter struct {
	ID    string `bson:"_id"`
	Value int64  `bson:"value"`
}
