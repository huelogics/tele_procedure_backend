package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type InternalTherapist struct {
	ID           primitive.ObjectID `bson:"_id,omitempty"`
	TherapistID  string             `bson:"therapist_id" json:"therapist_id"`
	Email        string             `bson:"email" json:"email" validate:"required,email"`
	Name         string             `bson:"name" json:"name" validate:"required"`
	Password     string             `bson:"password" json:"password,omitempty"`
	Organization string             `bson:"organization" json:"organization" validate:"required"`
	IsSuperUser  bool               `bson:"super_user" json:"-"`
	RefreshToken string             `bson:"refresh_token,omitempty" json:"-"`
	CreatedAt    time.Time          `bson:"created_at" json:"createdAt,omitempty"`
	UpdatedAt    time.Time          `bson:"updated_at" json:"updatedAt,omitempty"`
}

type Procedure struct {
	ID            primitive.ObjectID `bson:"_id,omitempty"`
	ProcedureId   string             `bson:"procedure_id" json:"procedure_id"`
	Name          string             `bson:"name" json:"name" validate:"required"`
	Options       []string           `bson:"options" json:"options"`
	Landmarks     []string           `bson:"landmarks" json:"landmarks"`
	Joints        [][]string         `bson:"joints" json:"joints"`
	Details       string             `bson:"details" json:"details"`
	IdealRange    float64            `bson:"ideal_range" json:"ideal_range"`
	PivotLandmark string             `bson:"pivot_landmark" json:"pivot_landmark"`
	TherapistId   string             `bson:"therapist_id" json:"therapist_id"`
}

type ProcedureResponse struct {
	Name          string     `json:"name"`
	Options       []string   `json:"options"`
	Landmarks     []string   `json:"landmarks"`
	Joints        [][]string `json:"joints"`
	Details       string     `json:"details"`
	IdealRange    float64    `json:"ideal_range"`
	PivotLandmark string     `json:"pivot_landmark"`
}

type CVProcedure struct {
	ID            primitive.ObjectID `bson:"_id,omitempty"`
	ProcedureId   string             `json:"procedure_id" bson:"procedure_id"`
	CVProcedureId string             `bson:"cv_procedure_id" json:"cv_procedure_id"`
	Type          []OptionType       `json:"type"`
}

type OptionType struct {
	Key   string        `json:"key"`
	Value LandmarkJoint `json:"value"`
}

type LandmarkJoint struct {
	Key         string   `json:"key"`
	Value       []string `json:"value"`
	Coordinates []string `json:"coordinates"`
}

type Exercise struct {
	ID          primitive.ObjectID `bson:"_id,omitempty"`
	ExerciseId  string             `bson:"exercise_id" json:"exercise_id"`
	Name        string             `bson:"name" json:"name" validate:"required"`
	TherapistID string             `bson:"therapist_id" json:"therapist_id"`
}

type TokenResponse struct {
	AccessToken  string `bson:"access_token,omitempty"`
	RefreshToken string `bson:"refresh_token,omitempty"`
}

type LoginInfoData struct {
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required"`
}
