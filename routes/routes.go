package routes

import (
	"procedure-backend/controllers"
	"procedure-backend/middleware"

	"github.com/gin-gonic/gin"
)

func ProcedureRoutes(router *gin.Engine) {

	router.POST("/sign_up", controllers.SignUpHandler())
	router.POST("/sign_in", controllers.SignInHandler())
	router.Use(middleware.Authenticate())
	{
		router.POST("/refresh_token", controllers.RefreshTokenHandler())
		router.POST("/sign_out", controllers.SignOutHandler())
		router.POST("/add_procedure", controllers.AddProcedureHandler())
		router.GET("/get_all_procedures", controllers.GetAllProceduresHandler())
		router.GET("/get_procedure/:procedure_id", controllers.GetProcedureHandler())
		router.GET("/cv_procedure/:procedure_id", controllers.CVProcedureHandler())
		router.GET("/get_all_cv_procedures", controllers.GetAllCVProceduresHandler())
		router.GET("/export_cv_procedures", controllers.ExportCVProceduresToJSONHandler())
		router.POST("/create_icf_core_set_record", controllers.CreateICFCORESETRecord())
		router.GET("/get_records_added_by_therapist", controllers.GetRecordsAddedByTherapist())
		router.GET("/get_record_by_case_number/:case_number", controllers.GetRecordsByCaseNumber())
		router.PUT("/update_record_by_case_number/:case_number", controllers.UpdateRecordsAddedByTherapistAndSuperUser())
		router.POST("/add_exercise", controllers.AddExerciseHandler())
		router.GET("/get_all_exercises", controllers.GetAllExercisesHandler())
		router.GET("/get_exercise/:exercise_id", controllers.GetExerciseHandler())
		router.PATCH("/edit_exercise/:exercise_id", controllers.EditExerciseHandler())

	}
}
