package httputil

type HTTPMessage struct {
	Code    int         `json:"code"`
	Message interface{} `json:"message"`
}
