# Start from the official Golang image
FROM golang:1.20-alpine AS builder

# Set necessary environment variables for Go
ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64
    

# Move to working directory /build
WORKDIR /build

# Copy and download dependency using go mod
COPY go.mod .
COPY go.sum .
RUN go mod download

# Copy the code into the container
COPY . .



# Build the application
RUN go build -o main .

# Start a new stage from scratch
FROM alpine:latest

# Set the Current Working Directory inside the container
WORKDIR /app

# Copy .env file
COPY .env /app

# Copy the Pre-built binary file from the previous stage
COPY --from=builder /build/main .

# Expose port 4001 to the outside world
EXPOSE 4011

# Command to run the executable
CMD ["./main"]